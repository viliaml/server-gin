FROM alpine:3.17

WORKDIR /

COPY server ./

EXPOSE 8080

ENTRYPOINT ["/server"]