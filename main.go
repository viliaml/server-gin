package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	_ "server-gin/docs"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"golang.org/x/exp/slices"
)

var appVersion, commit, buildTime string
var versions Versions
var censors = make(map[string]bool)
var mu sync.Mutex
var config Config
var validate *validator.Validate

var (
	httpRequestCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "The total number of http requests",
	}, []string{"code", "url"})
)

// @title     Golang Bookstore API
// @version         1.0.0
// @description     A book management service API in Go using Gin framework.
// @contact.name   Viliam L
// @contact.url    https://twitter.com/
// @host      localhost:8080
// @BasePath  /api/v1
func main() {
	versions.Version = appVersion
	versions.Commit = commit
	versions.BuildTime = buildTime

	// set config from file
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	defer viper.WriteConfig()
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		log.Fatal("fatal error config file: %w", err)
	}

	err = viper.Unmarshal(&config)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	validate = validator.New()

	err = validate.RegisterValidation("author", func(fl validator.FieldLevel) bool {
		value := fl.Field().String()
		regex := regexp.MustCompile(`^OL[0-9]+A$`)
		return regex.MatchString(value)

	})
	if err != nil {
		log.Fatalf("author is not in required format, %v", err)
	}

	err = validate.Struct(config)
	if err != nil {

		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println(err)
			return
		}

		for _, err := range err.(validator.ValidationErrors) {

			fmt.Println(err.StructField())
			fmt.Println(err.Tag(), err.ActualTag())
			fmt.Println(err.Kind(), err.Type())
			fmt.Println(err.Value())
		}

		// from here you can create your own error messages in whatever language you wish
		return
	}

	setCensors(config.Censors)

	r := gin.Default()
	r.Use(incCounter)

	r.GET(config.Server.BaseApiUrl+"/v1/version", version)
	r.GET(config.Server.BaseApiUrl+"/v1/authors", getAuthors)
	r.GET(config.Server.BaseApiUrl+"/v1/books", getBooks)
	r.POST(config.Server.BaseApiUrl+"/v1/censors", enpSetCensors)
	r.GET(config.Server.MetricsUrl, gin.WrapH(promhttp.Handler()))

	r.GET(config.Server.SwaggerUrl, ginSwagger.WrapHandler(swaggerFiles.Handler))

	srv := &http.Server{
		Addr:    config.Server.Addr,
		Handler: r,
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	log.Print("Server Started")

	<-done
	log.Print("Server Stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second) // waits 20 sec before exit
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}

	log.Print("Server Exited Properly")

}

// version             godoc
// @Summary      Get app version
// @Description  Returns the version, commit hash and build time.
// @Tags         version
// @Produce      json
// @Success      200  {array}  Versions
// @Router       /version [get]
func version(c *gin.Context) {
	time.Sleep(1 * time.Second)
	c.IndentedJSON(http.StatusOK, Versions{Version: versions.Version, Commit: versions.Commit, BuildTime: versions.BuildTime})
}

// getAuthors             godoc
// @Summary      Get authors of book
// @Description  Returns the authors of specified book.
// @Tags         book
// @Produce      json
// @Success      200  {array}  RespBooks
// @Failure      400  {object} ErrorResponse
// @Failure      500  {object} ErrorResponse
// @Router       /authors [get]
// @Param        book query string false "ISBN Book"
func getAuthors(c *gin.Context) {
	bookAuthors := Book{}
	name := Name{}

	book := c.Query("book")
	fmt.Println("book:", book)
	if book == "" {
		c.IndentedJSON(http.StatusBadRequest, ErrorResponse{Error: "missing URL param book which has to contain isbn of book"})
		return
	}

	// TODO: ISBN regex check

	url := "https://" + config.OpenLibrary.BaseUrl + "/isbn/" + book + ".json"

	body, err := apiCall(url)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}

	err = json.Unmarshal(body, &bookAuthors)
	if err != nil {
		fmt.Println("error:", err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}

	respA := make([]RespAuthor, len(bookAuthors.Authors))

	for index := range bookAuthors.Authors {
		url = "https://" + config.OpenLibrary.BaseUrl + bookAuthors.Authors[index].Key + ".json"
		body, err = apiCall(url)
		if err != nil {
			c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
			return
		}
		err := json.Unmarshal(body, &name)
		if err != nil {
			fmt.Println("error:", err)
			c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
			return
		}

		bookAuthors.Authors[index].Key = strings.ReplaceAll(bookAuthors.Authors[index].Key, "/authors/", "")

		respA[index].Name = name.Name
		respA[index].Key = bookAuthors.Authors[index].Key
	}

	c.IndentedJSON(http.StatusOK, respA)

}

func getBooks(c *gin.Context) {
	authorBooks := Entries{}

	author := c.Query("author")
	if author == "" {
		c.IndentedJSON(http.StatusBadRequest, ErrorResponse{Error: "missing URL param book which has to contain isbn of book"})
		return
	}
	if isCensored(author) {
		c.IndentedJSON(http.StatusForbidden, ErrorResponse{Error: "this author is censored and his/her books will not be shown"})
		return
	}

	limit := strconv.FormatUint(uint64(config.OpenLibrary.ResultLimit), 10)

	url := "https://" + config.OpenLibrary.BaseUrl + "/authors/" + author + "/works.json?limit=" + limit

	body, err := apiCall(url)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}

	err = json.Unmarshal(body, &authorBooks)
	if err != nil {
		fmt.Println("error:", err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}

	respB := make([]RespBooks, len(authorBooks.Entries))

	for index := range authorBooks.Entries {

		respB[index].Name = authorBooks.Entries[index].Title
		respB[index].Key = authorBooks.Entries[index].Key
		respB[index].Revision = authorBooks.Entries[index].Revision
		respB[index].PublishDate = authorBooks.Entries[index].Created.Value
	}

	c.IndentedJSON(http.StatusOK, respB)
}

func enpSetCensors(c *gin.Context) {

	reqCensors := []string{}

	err := c.BindJSON(&reqCensors)
	if err != nil {
		log.Println(err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}
	setCensors(reqCensors)
	c.Status(http.StatusOK)

	fmt.Println(censors)
}

func setCensors(cens []string) {
	mu.Lock()
	for _, c := range cens {
		censors[c] = true
		if !slices.Contains(config.Censors, c) {
			config.Censors = append(config.Censors, c)
		}
	}
	viper.Set("censors", config.Censors)
	viper.WriteConfig()
	mu.Unlock()
}

func isCensored(author string) bool {
	mu.Lock()
	defer mu.Unlock()
	return censors[author]
}

func apiCall(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}

	defer resp.Body.Close()

	// !!! nacitava cely []byte do pamate, lepsie bufio.NewReader()
	body, err := ioutil.ReadAll(resp.Body)

	return body, err
}

func incCounter(c *gin.Context) {
	url := c.Request.URL.String()
	c.Next()
	code := strconv.Itoa(c.Writer.Status())

	//httpRequestCounter.WithLabelValues(code, url).Inc()
	counter, err := httpRequestCounter.GetMetricWith(prometheus.Labels{"code": code, "url": url})
	if err != nil {
		log.Println(err)
	} else {
		counter.Inc()
	}

}

// merge request try
// curl localhost:8080/api/v1/books?author=OL1394244A
